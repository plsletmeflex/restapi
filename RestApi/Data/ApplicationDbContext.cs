﻿using Microsoft.EntityFrameworkCore;
using RestApi.Models;

namespace RestApi.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Message> Messages { get; set; }
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            messageModelBuilder(modelBuilder);
        }

        private void messageModelBuilder(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>()
                .Property(t => t.Text)
                .IsRequired()
                .HasColumnType("varchar(1000)");
        }
    }
}